/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
/**
 *Une bean "Personne"
 * @author Francis RAHARISON
 */
public class Personne implements Serializable{
   
    private int id;
   
   @NotBlank(message = "Le non ne peuxt pas être vide")
   @Size(max=30,message = "Le nom contient pas plus de 30 caractères")
   @Size(min=2,message ="Le nom contient au moins 2 caractères ")
   private String nom;
   
   @NotBlank(message = "Le prénom ne doit pas être vide")
   @Size(max=20,message = "Le prénom contient pas plus de 20 Caractères")
   @Size(min=2, message="Le prénom contient pas plus de 20 Caractères")
   private String prenom;
   

    public Personne(String nom, String prenom) {
       // this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }
    
     public Personne(int id,String nom,String prenom){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        
    }

    public Personne() {
    }

//    public Personne(String sonNom, String sonPrenom) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    public int getId() {
        return id;
   }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
}
