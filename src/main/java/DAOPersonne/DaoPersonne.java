/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOPersonne;

import controllers.ListePersonneController;
import controllers.PageAcceuilController;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import models.Personne;
import frontController.ServletFrontController;
import java.util.List;

/**
 *
 * @author pc
 */
public class DaoPersonne {
    static ResultSet rst;
    private static final Connection CONNECTION = ServletFrontController.connection; 
    // METHODE POUR INSTANCIER UNE PERSONNE DEPUIS UN ENREGISTREMENT
    //DE LA TABLE PERSONNE DE LA BD
    public static Personne resultSetPersonne(ResultSet rst) throws SQLException, Exception {
        Personne personne = new Personne();
        personne.setId(rst.getInt("id"));
        personne.setNom(rst.getString("nom"));
        personne.setPrenom(rst.getString("prenom"));
        return personne;
    }
     //DE LA FONCTION findAll-Pour afficher la table Client de la BD Reverso
    public static List<Personne> findAll() throws SQLException{
       Statement stmt=null;
       String query = "SELECT * FROM tbpersonne";
       try{
//       CONNECTION = ServletFrontController.datasource.getConnection();   
       stmt = CONNECTION.createStatement();
       rst=stmt.executeQuery(query);
        if(PageAcceuilController.getpersonneListe().isEmpty()){
           while (rst.next()) { 
               Personne personne = new Personne();
               personne = resultSetPersonne(rst);
               PageAcceuilController.getpersonneListe().add(personne);
               int id = rst.getInt("id");
                System.out.print(id + "\t");
                System.out.print(rst.getString("nom") + "\t");
                System.out.print(rst.getString("prenom") + "\t");
                System.out.println("");
           }
           }
       }catch(Exception ex){
          ex.printStackTrace();
       }
        finally {
            if (stmt != null) {
                stmt.close();
            }
     }
       return PageAcceuilController.getpersonneListe();
}
}