/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontController;

import controllers.AjoutPersonneController;
import controllers.ICommand;
import controllers.ListePersonneController;
import controllers.ListePersonneGroupeController;
import controllers.ListePersonneJouantInstrumentController;
import controllers.ModifPersonneController;
import controllers.PageAcceuilController;
import controllers.SupprPersonneController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import models.Personne;
/**
 *
 * @author Francis RAHARISON
 */
@WebServlet(name = "ServletFrontController", urlPatterns = {"/ServletFrontController"})
public class ServletFrontController extends HttpServlet {
private Map instruction = new HashMap();
String urlActuel="";
int nbPagesVues;
private static final Logger LOGGER = Logger.getLogger(Personne.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

public static Connection connection;

@Resource(name = "jdbc/bdProjetjee1")
public static DataSource datasource;

@Override
public void init() {
    instruction.put(null,new PageAcceuilController());
    instruction.put("page1",new AjoutPersonneController());
    instruction.put("page2",new ModifPersonneController());
    instruction.put("page3",new SupprPersonneController());
    instruction.put("page4",new ListePersonneController());
    instruction.put("page5",new ListePersonneGroupeController());
    instruction.put("page6",new ListePersonneJouantInstrumentController());
    instruction.put("page7",new PageAcceuilController());
    
    try{
     connection = datasource.getConnection();
     LOGGER.warning("OK la connection  à la bdd");
    }catch(SQLException e){
        LOGGER.severe("on'ap pas de connection à la bdd");
     new IOException("On peut pas ouvrir la connection");
    }
}
@Override
//Fermer la demande de connection
public void destroy(){
    try{
      connection.close();
    }catch(SQLException e){
        new IOException("On ne peut pas fermer la demande de  connection");
    }
}
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                String cmd = request.getParameter("cmd");
                 HttpSession session = request.getSession();
                 if (session.getAttribute("nbPagesVues") == null) {
                  session.setAttribute("nbPagesVues", 0);
 }
                
        try{
            //recupere la valeur de la clé corréspondate à cmd)
        ICommand com = (ICommand)instruction.get(cmd);
        //appel method du controleur qui fait la liaison avec le fichier JSP
        urlActuel = com.execute(request, response);
        
        }catch(Exception e){
         request.setAttribute("erreur", "Une erreur est intervenue");
         urlActuel="erreur.jsp";
        }
    finally {
            //FrontControleur dispacthe l'URL à la vue 
            request.getRequestDispatcher("/WEB-INF/JSP/"+urlActuel).forward(request,response);
    }
   
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
