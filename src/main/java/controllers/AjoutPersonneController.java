/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import models.Personne;
import models.forms.SaisiePersonneForm;

/**
 *
 * @author Francis RAHARISON
 */
public class AjoutPersonneController implements ICommand {
//Integer nbPagesVues ;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String messNom = "";
        String messPrenom = "";

        HttpSession session = request.getSession();
        Integer nbPagesVues = (Integer) (session.getAttribute("nbPagesVues"));
        nbPagesVues++;
        session.setAttribute("nbPagesVues", nbPagesVues);
        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("monCookie")) {
                    request.setAttribute("monCookie", URLDecoder.decode(cookie.getValue(), "UTF-8"));
                }
            }
        }
        if (!request.getParameterMap().containsKey("nom")) {
        }
        if (request.getParameterMap().containsKey("nom")) {
            request.setAttribute("valeurNom", request.getParameter("nom"));
            request.setAttribute("valeurPrenom", request.getParameter("prenom"));

//        HttpSession session = request.getSession(); 
//        Integer nbPagesVues=(Integer) (session.getAttribute("nbPagesVues"));
//       // session.getAttribute("nbPagesVues ");
//        nbPagesVues++;
//        session.setAttribute("nbPagesVues",nbPagesVues);
            //  Recupérer les cookies
//            Cookie cookies [] = request.getCookies();
//            if (cookies != null) {
//                for (Cookie cookie : cookies) {
//                    if (cookie.getName().equals("monCookie")) {
//                        request.setAttribute("monCookie", URLDecoder.decode(cookie.getValue(), "UTF-8"));
//                    }
//                }
//            }
            String sonNom = request.getParameter("nom");
            String sonPrenom = request.getParameter("prenom");
            Personne personne = new Personne();
            personne.setId(PageAcceuilController.getpersonneListe().size() + 1);
            personne.setNom(sonNom);
            personne.setPrenom(sonPrenom);
            
            //DEBUT VALIDATION DE LA SAISIE PERSONNE AVEC LAPI BEANS VALIDATION
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            //personne.setId(0);
            Set<ConstraintViolation<Personne>> violations = validator.validate(personne);
            
            if (violations.isEmpty()) {
                SaisiePersonneForm verifSaisie = new SaisiePersonneForm();
                 verifSaisie.verifieForm(request);
                 String message = verifSaisie.getResultat();
                
                 if(message.isEmpty()){
                 PageAcceuilController.getpersonneListe().add(personne);
                 }else{
                   request.setAttribute("message",message);   
                   request.setAttribute("valeurNom",request.getParameter("nom"));
                   request.setAttribute("valeurPrenom",request.getParameter("prenom"));
                 }
            } else {
                for (ConstraintViolation<Personne> violation : violations) {
                    if (violation.getPropertyPath().toString().equals("nom")) {
                        messNom = messNom + violation.getMessage() + " ";
                    } else if (violation.getPropertyPath().toString().equals("prenom")) {
                        messPrenom = messPrenom + violation.getMessage() + " ";
                    }
                }
//           
            }
            
            request.setAttribute("erreurNom", messNom);
            request.setAttribute("erreurPrenom", messPrenom);

        }

        return "ajoutPersonne.jsp";
    }

}

//DEBUT VALIDATION DE LA SAISIE PERSONNE AVEC LAPI BEANS VALIDATION
//             ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//             Validator validator = factory.getValidator();
//             personne.setId(0);
//             Set<ConstraintViolation<Personne>> violations = validator.validate(personne);
//Si la collection d'erreur est vide donc pas d'erreur de remonté
//et valider le formulaire avec la classe VerifPersoForm
//             if(violations.isEmpty()){
//Appeller la méthode verifForm de la classe VerifPersForm
//verifie si le formuleire est valide sinon attribut une value
//             verifPersForm.verifForm(request);
//             message = verifPersForm.getResult();
//             }
//             //sinon on concataine les erreurs recuperées par le bean dans la variable message   
//       ){
//                 message = message + violation.getMessage() + "<br>";
//             }
//             }
//             //Si erreur dans le bean ou formulaire non validé on renvoie la JSP avec le 
//             if(!verifPersForm.getResult().isEmpty()) || (!violations.isEmpty())){
//            //si erreur redonne la valeur dèjé saisie à la JSP
//            request.setAttribute("annonceNom",nom);
//            request.setAttribute("annoncePrenom",prenom);
//        }
//FIN VALIDATION DE LA SAISIE PERSONNE AVEC LAPI BEANS VALIDATION
//        }
//            } else {
//               for (ConstraintViolation violation<Personne> : violations) {
//                message = message + violation.getMessage() + "<br>";
//            }  
