/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Personne;

/**
 *
 * @author Francis RAHARISON
 */
public class ModifPersonneController implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
         HttpSession session = request.getSession(); 
        Integer nbPagesVues=(Integer) (session.getAttribute("nbPagesVues"));
        nbPagesVues++;
        session.setAttribute("nbPagesVues",nbPagesVues);
        Cookie cookies [] = request.getCookies();
            if(cookies != null){
                for(Cookie cookie: cookies){
                    if(cookie.getName().equals("monCookie")){
                        request.setAttribute("monCookie", URLDecoder.decode(cookie.getValue(), "UTF-8"));
                    }
                }
            }
        if (request.getParameterMap().containsKey("selecteur") == false
                && request.getParameterMap().containsKey("id") == false) {
            request.setAttribute("tousLesAdherents", PageAcceuilController.getpersonneListe());
        } else if (request.getParameterMap().containsKey("selecteur") == true) {
            int id = Integer.parseInt(request.getParameter("selecteur"));
            String sonPrenom = null;
            String sonNom = null;
            List laListe = PageAcceuilController.getpersonneListe();
            for (int i = 0; i < laListe.size(); i++) {
                Personne laPersonne = (Personne) laListe.get(i);
                //if (sonNom == laPersonne.getNom()) {
                if (laPersonne.getId() == id) {
                    sonPrenom = laPersonne.getPrenom();
                    sonNom = laPersonne.getNom();

                }
            }
            Personne personneModif = new Personne(id, sonNom, sonPrenom);
            request.setAttribute("laPersonneModif", personneModif);
        } else if ((request.getParameterMap().containsKey("nom") == true)
                && (request.getParameterMap().containsKey("prenom") == true)) {
            // if(request.getParameterMap().containsKey("formulaireModif") == true)
            String sonNom = request.getParameter("nom");
            String Prenom = request.getParameter("prenom");
            int id = Integer.parseInt(request.getParameter("id"));
            Personne persononneMod = new Personne();
            persononneMod.setNom(sonNom);
            persononneMod.setPrenom(Prenom);
            persononneMod.setId(id);
            for (int i = 0; i < PageAcceuilController.getpersonneListe().size(); i++) {
                Personne personne = (Personne) PageAcceuilController.getpersonneListe().get(i);
                if (persononneMod.getId() == personne.getId()) {
                    PageAcceuilController.getpersonneListe().remove(i);
                }
            }
            PageAcceuilController.getpersonneListe().add(persononneMod);
             request.setAttribute("newPersonne",persononneMod);
        }

        return "modifPersonne.jsp";
    }
}
