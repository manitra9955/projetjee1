/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;


import DAOPersonne.DaoPersonne;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Personne;
import Utilitaires.CreerListe;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Francis RAHARISON
 */
public class ListePersonneController implements ICommand{
//    private static  List<Personne> personneListe = new ArrayList(); 
//    DaoPersonne personneDao = new DaoPersonne();
    
   
    @Override
     public String execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
//        if(personneListe.isEmpty()){
//        Personne personne1 = new Personne(1,"Richard", "Pierre");
//        Personne personne2 = new Personne(2,"Delon", "Alain");
//        Personne personne3 = new Personne(3,"Stallone","Silvester");
//       
//        personneListe.add(personne1);
//        personneListe.add(personne2);
//        personneListe.add(personne3);
//        }
        HttpSession session = request.getSession(); 
        Integer nbPagesVues=(Integer) (session.getAttribute("nbPagesVues"));
        nbPagesVues++;
        session.setAttribute("nbPagesVues",nbPagesVues);
     
        
          request.setAttribute("tousLesAdherents",DaoPersonne.findAll());
        //request.setAttribute("tousLesAdherents",personneListe);
                
       return "affichagePersonne.jsp";
     }
//     public static List<Personne> getpersonneListe() {
//        return personneListe;
//    }
     
}
