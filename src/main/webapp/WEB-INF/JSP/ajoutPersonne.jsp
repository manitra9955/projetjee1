<%-- 
    Document   : ajoutPersonne
    Created on : 3 juil. 2020, 20:57:09
    Author     : Francis RAHARISON
--%>

<%@page import="controllers.ListePersonneController"%>
<%@page import="models.Personne"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="styleCss/navbarStyle.css" type="text/css">
        <script src="https://kit.fontawesome.com/a0c8b86555.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <c:import url="/WEB-INF/JSP/barreDeMenu.jsp"></c:import>
            <h2> CREER DES ADHERENTS </h2>
            <p>Nombre de pages vues: ${nbPagesVues}</p>
        <p>Le cookie est: ${monCookie}<p>
       
        


        <form  method="post">  

            <div class="col-md-4 mb-3 ">
                <label for="name">Nom :</label>
                <input class="form-control " type="text" id="name" name="nom" value="${valeurNom}"> 
                <c:if test="${!empty erreurNom}">
                <p>Erreur nom: ${erreurNom}<p>
                </c:if>
            </div >
            <div class="col-md-4 mb-3">
                <label for="prenom">Prenom:</label>   
                <input class="form-control " type="text" id="prenom" name="prenom" value="${valeurPrenom}">
                 <c:if test="${!empty erreurPrenom}">
                 <p>Erreur prenom: ${erreurPrenom}<p>
                     
                </c:if>
               
            </div>
                <c:if test="${!empty message}">
                 <p>Erreur:${message}<p>
                 </div>  
                 </c:if>
            <div class="col-md-4 mb-3">
                <button type="submit">Valider</button>    
            </div>
                 
        </form>





        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    </body>
</html>
