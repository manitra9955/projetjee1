<%-- 
    Document   : navbar
    Created on : 3 juil. 2020, 23:19:27
    Author     : Francis RAHARISON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>Menu de Navigateur</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="styleCss/navbarStyle.css" type="text/css">
        <script src="https://kit.fontawesome.com/a0c8b86555.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <!--NavBar-->

        <nav class="navbar  navbar-expand-md navbar-light bg-dark">
            <ul class="navbar-nav">

                <li class="nav-item" > <a class="nav-link text-light font-weight-bold 
                                          text-uppercase px-3" href="#">Home</a></li>
                <li class="nav-item "> <a class="nav-link text-light font-weight-bold
                                          text-uppercase px-3" href="#">Contact</a></li>
                <li class="nav-item dropdown"> <a class="nav-link text-light font-weight-bold 
                                          text-uppercase px-3 dropdown-toggle" href="#" data-toggle="dropdown">Navigation</a>
                
                    <div class="dropdown-menu"> 
                        <a class="dropdown-item" href="?cmd=page1">Ajout</a>
                         <a class="dropdown-item" href="?cmd=page2">Modification</a>
                         <a class="dropdown-item" href="?cmd=page3">Suppression</a>
                         <a class="dropdown-item" href="?cmd=page4">Liste des Adhérents</a>
                         <a class="dropdown-item" href="?cmd=page5">Liste personnes d'un groupe</a>
                         <a class="dropdown-item" href="?cmd=page6">Liste personnes jouant d'un instrument</a>
                     </div>
                </li>
                
                <li class="nav-item "> <a class="nav-link text-light font-weight-bold 
                                          text-uppercase px-3" href="#">Loging</a></li>
               </ul>
            <form class="form-inline">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search"/>
                </div>
                <div class="input-group-append">
                    <button type="button" class="btn bg-light"><i class="fas fa-search"></i></button>
                </div>
            </form>
        </nav>
        <!--END of NavBar-->
        <header id="tete">

        </header>
        <section>
      
        </section>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    </body>
</html>
