<%-- 
    Document   : affichagePersonne
    Created on : 3 juil. 2020, 21:01:19
    Author     : Francis RAHARISON
--%>

<%@page import="models.Personne"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="styleCss/navbarStyle.css" type="text/css">
        <script src="https://kit.fontawesome.com/a0c8b86555.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <c:import url="/WEB-INF/JSP/barreDeMenu.jsp"></c:import>
        <h2>TOUS LES ADHERENTS</h2>
        <p>Nombre de pages vues: ${nbPagesVues}</p>
        <!-- <c:out value="${monCookie}"/>-->
        <!--Début Table Adhérents-->
        <div class="col-md-6">
            <table class="table table-dark"  >
                <thead>
                    <tr>
                        <!--  <th scope="col">Id</th>-->
                        <th scope="col">Nom</th>
                        <th scope="col">Prenom</th>
                    </tr>
                </thead>
                <tbody>

                    <!--   <tr>
                         <th scope="row">1</th>
                         <td></td>
                         <td></td>
                       </tr>
                       <tr>
                         <th scope="row">2</th>
                         <td></td>
                         <td></td>
                       </tr>-->

                    <%
                        //   ArrayList laListe = (ArrayList) request.getAttribute("tousLesAdherents");
                        // if (laListe.isEmpty()) {
                        //    out.println("<tr><td>" + "La liste est vide" + "</td>" + "</tr>");
                        // } else {
                        //     for (int i = 0; i < laListe.size(); i++) {
                        //         Personne laPersonne = (Personne) laListe.get(i);
                        //         //  out.println("<tr><td>" + laPersonne.getId()+ "</td>");
                        //         out.println("<tr><td>" + laPersonne.getNom() + "</td>");
                        //         out.println("<td>" + laPersonne.getPrenom() + "</td>");
                        //         out.println("</tr>");

                        //       }
                        //   }
                    %>
                    <c:choose>  
                    <c:when test="${tousLesAdherents == null}">
                    <h1>La liste est vide</h1>
                     </c:when>
                    <c:otherwise>
                    <c:forEach items="${tousLesAdherents}" var="tousLesAdherents">
                    <tr>
                        <td>${tousLesAdherents.nom} </td>
                        <td>${tousLesAdherents.prenom} </td>
                    </tr>
                    </c:forEach>
                    </c:otherwise> 
                    </c:choose>
               
                </tbody>
            </table>
        </div>
        <!--Fin Table Adhérents-->



        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>

    </body>
</html>
