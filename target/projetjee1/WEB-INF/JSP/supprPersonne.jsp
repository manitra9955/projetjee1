<%-- 
    Document   : supprPersonne
    Created on : 3 juil. 2020, 20:58:17
    Author     : Francis RAHARISON
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="controllers.ListePersonneController"%>
<%@page import="models.Personne"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>gg</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="styleCss/navbarStyle.css" type="text/css">
        <script src="https://kit.fontawesome.com/a0c8b86555.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <c:import url="/WEB-INF/JSP/barreDeMenu.jsp"></c:import>
         <!--DEBUT JSTL-->
          <p>Nombre de pages vues: ${nbPagesVues}</p>
            <c:if test="${!empty tousLesAdherents}">
            <h2>SUPPRIMER DES ADHERENTS</h2>
            <form  method='post' action='ServletFrontController?cmd=page3'>
                <label for=''>Choisir l'adhérent à supprimer</label><br><br>
                <select  name='selecteur' id='selecteur'>
                    <c:forEach items="${tousLesAdherents}" var="tousLesAdherents">  
                        <option value='${tousLesAdherents.id}'> ${tousLesAdherents.nom} </option>
                    </c:forEach>
                </select>
                <button type='submit'>Valider</button>
            </form>
        </c:if>  
       
            <c:if test="${!empty laPersonneModif}">
        
            <h2> MODIFIER DES ADHERENTS </h2>
            <form name ='formulaireModif' method='post'>
                <label for='nom'>Nom:</label>
                <input value ="${laPersonneModif.nom}" name='nom'><br>
                <label for='prenom'>Prenom:</label>
                <input value ="${laPersonneModif.prenom}" name='prenom'><br>
                <input value = "${laPersonneModif.id}" name='id' type='hidden'><br>
                <button type='submit'>Valider</button>
            </form>
        </c:if>
        <c:if test="${!empty supprPersonne}"> 
            <h4>La suppression a été effectuée avec succes</h4>   
        </c:if>
        <!--FIN JSTL-->
        <%
        //    if (request.getParameterMap().containsKey("nom") == false
        //            && request.getParameterMap().containsKey("selecteur") == false) {
        //        List laListe = (ArrayList) request.getAttribute("tousLesAdherents");
       //         out.println(" <h2>MODIFIER DES ADHERENTS</h2>");
       //         out.println("<form  method='post' action='ServletFrontController?cmd=page3'>");
       //         out.println(" <label for=''>Choisir l'adhérent à supprimer</label><br><br>");
       //         out.println("<select  name='selecteur' id='selecteur'>");

       //         for (int i = 0; i < laListe.size(); i++) {
       //             Personne personne = new Personne();
      //              personne = (Personne) laListe.get(i);
      //              out.println("<option value='" + personne.getId() + "'>" + personne.getNom() + "</option>");
      //          }
      //          out.println("</select>");
      //          out.println(" <button type='submit'>Valider</button>");
      //          out.println(" </form>");
      //      } else if (request.getParameterMap().containsKey("selecteur") == true) {
      //          Personne personneModif = (Personne) request.getAttribute("laPersonneModif");
      //          String nomP = personneModif.getNom();
      //          String prenomP = personneModif.getPrenom();
      //          int id = personneModif.getId();
      //          out.println(" <h2> SUPPRIMER DES ADHERENTS </h2>");
      //          out.println("<form name ='formulaireModif'"
       //                 + " method='post'>");
      //          //action='ServletFrontController?cmd=page2'
      //          out.println(" <label for='nom'>Nom:</label>");
      //          //   out.println("<input name='nom' value='" + nomP + " ' "">);
      //          out.println("<input value ='" + nomP + "' name='nom'><br>");
       //         out.println(" <label for='prenom'>Prenom:</label>");
       //         out.println("<input value ='" + prenomP + "' name='prenom'><br>");

       //         out.println("<input value ='" + id + "' name='id' type='hidden'><br>");
       //         out.println("<button type='submit'>Valider</button>");
       //         out.println("</form>");
       //     } else {
       //         //JOptionPane.showConfirmDialog(null,"Voulez-vous ","Suppression",1);
                
        //        out.println("<h4>La suppression a été effectuée avec succes</h4>");
        //    }
            
        %>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    </body>
</html>




<!--
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Suppression</title>
    </head>
    <body>
        <h1>SUPPRESSION DES ADHERENTS</h1>
    </body>
</html>-->
